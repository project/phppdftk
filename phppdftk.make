api = 2
core = 7.x

libraries[php-tmpfile][download][type] = "file"
libraries[php-tmpfile][download][url] = "https://github.com/mikehaertl/php-tmpfile/archive/1.0.0.tar.gz"

libraries[php-shellcommand][download][type] = "file"
libraries[php-shellcommand][download][url] = "https://github.com/mikehaertl/php-shellcommand/archive/1.0.3.tar.gz"

libraries[phppdftk][download][type] = "file"
libraries[phppdftk][download][url] = "https://github.com/mikehaertl/php-pdftk/archive/0.5.0.tar.gz"
