<?php

/**
 * @file
 * Wrapper class for PDFTK.
 */

/**
 * Class Pdftk.
 *
 * Used as a wrapper class to \mikehaertl\pdftk\Pdf for backward compatibility.
 */
class PhpPdftk extends \mikehaertl\pdftk\Pdf {

  /**
   * {@inheritdoc}
   */
  public function __construct($pdf = NULL, array $options = array()) {
    parent::__construct($pdf, $options);
  }

}
