CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Maintainers


INTRODUCTION
------------

This module wraps php-pdftk which is a PDF conversion and form utility based on pdftk.

Brings the power of `pdftk` to drupal. Fill forms, split PDFs, add backgrounds or overlays, and more.

 * For a full description on php-pdftk visit:
  https://github.com/mikehaertl/php-pdftk

 * For a full description of the module visit:
  https://www.drupal.org/project/phppdftk

 * To submit bug reports and feature suggestions, or to track changes visit:
  https://www.drupal.org/project/issues/phppdftk


REQUIREMENTS
------------

The `pdftk` command must be installed on your system.

The `php-pdftk` library and it's dependencies must be installed in the `sites/all/library` directory.

This module requires the following modules: 

* Libraries (https://www.drupal.org/project/libraries)


INSTALLATION
------------

Ensure that `pdftk` is installed on your system.

Download the `php-pdftk` library. 

Enable the module.

MAINTAINERS
-----------

The 7.x-2.x branch was created by:

 * Claude Banman (cbanman) - https://www.drupal.org/u/cbanman

This module was created and sponsored by Acro Media, a drupal development company
in Kelowna, Canada.

 * Acro Media - https://www.acromedia.com/
 
This module was based off of `phpwkhtmltopdf`: https://www.drupal.org/project/phpwkhtmltopdf
